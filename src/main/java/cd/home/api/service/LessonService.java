package cd.home.api.service;

import cd.home.api.entitiy.Course;
import cd.home.api.entitiy.Lesson;
import cd.home.api.entitiy.Lesson;
import cd.home.api.repository.LessonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LessonService {
    @Autowired
    private LessonRepository lessonRepository;

    public List<Lesson> getAllLessons(String courseId) {
        return (List<Lesson>) lessonRepository.findCourseById(courseId);
    }

    public Optional<Lesson> getLesson(String id) {
        return lessonRepository.findById(id);
    }

    public void addLesson(Lesson Lesson) {
        lessonRepository.save(Lesson);
    }

    public void deleteLesson(String id) {
        List<Lesson> lessons = new ArrayList<Lesson>();
        lessonRepository.findAll().forEach(lessons::add);
        lessonRepository.delete(lessons.stream().filter(c -> c.getId().equals(id)).findFirst().get());
    }

    public void updateLesson(Lesson Lesson) {
        lessonRepository.save(Lesson);
    }


}
